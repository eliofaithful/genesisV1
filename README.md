![](elio-genesis-logo.png)

# genesis

An example app in progress for the **elioSin** tutorials.

**genesis** was generated by [generator-sin](https://gitlab.com/eliosinners/generator-sin)

## Requirements

- [elioSin Prerequisites](https://gitlab.com/elioway/eliosin/blob/master/doc/installing.md)

## Usage

Actually we suggest you don't use this repo at all! Just create your own "genesis" repo by following the tutorials in the order shown:

- [god Quickstart](https://gitlab.com/eliosinners/god/blob/master/doc/quickstart.md)
- [eve Quickstart](https://gitlab.com/eliosinners/eve/blob/master/doc/quickstart.md)
- [adon Quickstart](https://gitlab.com/eliosinners/adon/blob/master/doc/quickstart.md)

But if you get stuck with those tutorials, the **genesis** example app contains branches for each tutorial so you can see how it was suppose to turn out.

```shell
git clone https://gitlab.com/eliofaithful/genesis/
cd genesis
git checkout <tutorial-stage>
```

Where tutorial-stage can be any of the branches as follows:

- `git checkout hatched`

  - State immediately after when `yo sin` has finished running in an empty folder.

    - <https://gitlab.com/eliofaithful/genesis/tree/hatched>

  - `git checkout god-quickstarted`

    - _hatched_ then [god Quickstart](https://gitlab.com/eliosinners/god/blob/master/doc/quickstart.md) completed.

      - <https://gitlab.com/eliofaithful/genesis/tree/god-quickstarted>

    - `git checkout eve-quickstarted`

      - _god-quickstarted_ then [eve Quickstart](https://gitlab.com/eliosinners/eve/blob/master/doc/quickstart.md) completed.

        - <https://gitlab.com/eliofaithful/genesis/tree/eve-quickstarted>

      - `git checkout adon-quickstarted`

        - _eve-quickstarted_ then [adon Quickstart](https://gitlab.com/eliosinners/adon/blob/master/doc/quickstart.md) completed.

          - <https://gitlab.com/eliofaithful/genesis/tree/adon-quickstarted>

- `git checkout master`

  - _hatched_ then with new artwork for genesis + this README.md.

    - <https://gitlab.com/eliofaithful/genesis/>

## Seeing is believing

```shell
git clone https://gitlab.com/eliofaithful/genesis/
cd genesis
yarn
gulp
```

- [god Quickstart](https://gitlab.com/eliosinners/god/blob/master/doc/quickstart.md)

# gulp watch issues

If gulp crashes while running "watch", try this shell command in Linux.

```shell
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf
sudo sysctl -p
```

## Running Tests

```shell
yarn test
```

## Prettier

```shell
yarn format
```

## Contributor Group

- <https://gitlab.com/eliosin>

## License

[HTML5 Boilerplate](LICENSE.txt)

![](apple-touch-icon.png)
