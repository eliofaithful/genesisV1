// generated on 2019-05-25
var browserSync = require("browser-sync").create();
var gulp = require("gulp"),
  clean = require("gulp-clean-css"),
  concat = require("gulp-concat"),
  rename = require("gulp-rename"),
  sass = require("gulp-sass");

var css_folder = "css/";
var dcss_folder = "dist/css/";

var sass_watch = ["stylesheets/*.scss", "stylesheets/**/*.scss"];

// Compile sass into CSS & auto-inject into browsers
gulp.task("sass", function() {
  return gulp
    .src("stylesheets/judge.scss")
    .pipe(
      sass({
        includePaths: sass_watch
      })
    )
    .pipe(concat("genesis.css"))
    .pipe(gulp.dest(css_folder))
    .pipe(rename("genesis.min.css"))
    .pipe(clean())
    .pipe(gulp.dest(css_folder))
    .pipe(browserSync.stream());
});

var distro_css = ["css/normalize.css", "css/main.css", "css/genesis.css"];

// Minify css into CSS & auto-inject into browsers
gulp.task("clean", function() {
  return gulp
    .src(distro_css)
    .pipe(concat("genesis.min.css"))
    .pipe(clean())
    .pipe(gulp.dest(dcss_folder))
    .pipe(browserSync.stream());
});

// Static Server + watccing scss/html files
gulp.task("watch", ["sass", "clean"], function() {
  browserSync.init({
    server: "./"
  });
  gulp.watch(sass_watch, ["sass", "clean"]);
  gulp.watch(sass_watch).on("change", browserSync.reload);
  gulp.watch(["*.html"]).on("change", browserSync.reload);
});

gulp.task("default", ["watch"]);
